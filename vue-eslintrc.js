module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', 'eslint:recommended', '@vue/prettier'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'prefer-const': 'off',
  },
}

// "devDependencies": {
//   "@vue/cli-plugin-babel": "^4.2.0",
//   "@vue/cli-plugin-eslint": "^4.2.0",
//   "@vue/eslint-config-prettier": "^6.0.0",
//   "babel-eslint": "^10.0.3",
//   "eslint": "^6.7.2",
//   "eslint-plugin-prettier": "^3.1.1",
//   "eslint-plugin-vue": "^6.1.2",
//   "prettier": "^1.19.1",
// }
