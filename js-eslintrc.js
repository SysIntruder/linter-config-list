module.exports = {
  root: true,
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true,
    jest: true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    'prefer-const': 'off'
  }
}

// "devDependencies": {
//   "eslint": "^6.6.0",
//   "eslint-config-standard": "^14.1.0",
//   "eslint-plugin-import": "^2.18.2",
//   "eslint-plugin-node": "^10.0.0",
//   "eslint-plugin-promise": "^4.2.1",
//   "eslint-plugin-standard": "^4.0.1",
// }
