module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: 'react-app',
  plugins: ['import', 'jsx-a11y', 'prettier', 'react', 'react-hooks'],
  rules: {
    'prettier/prettier': 'error'
  }
}

// "devDependencies": {
//     "eslint": "^7.0.0",
//     "eslint-config-react-app": "^5.2.1",
//     "eslint-plugin-import": "^2.20.2",
//     "eslint-plugin-jsx-a11y": "^6.2.3",
//     "eslint-plugin-prettier": "^3.1.3",
//     "eslint-plugin-react": "^7.19.0",
//     "eslint-plugin-react-hooks": "^4.0.0",
//     "prettier": "^2.0.5"
// }