module.exports = {
  semi: false,
  singleQuote: true,
  printWidth: 75,
  tabWidth: 2,
}
