module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  extends: [
    'plugin:@typescript-eslint/recommended',
    'prettier/@typescript-eslint',
    'plugin:prettier/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  rules: {
    'prefer-const': 'off',
  }
}

// "devDependencies": {
//   "@typescript-eslint/eslint-plugin": "^2.17.0",
//   "@typescript-eslint/parser": "^2.17.0",
//   "eslint": "^6.8.0",
//   "eslint-config-prettier": "^6.9.0",
//   "eslint-plugin-prettier": "^3.1.2",
//   "prettier": "^1.19.1",
//   "typescript": "^3.7.5"
// }
